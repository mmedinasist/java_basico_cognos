//Negocio:
public class Alumno extends Persona {
    private int idAlumno;
    private String carrera;

    public Alumno(int idPersona, String nombre, String apPaterno, String apMaterno, int idAlumno, String carrera) {
        super(idPersona, nombre, apPaterno, apMaterno);
        this.idAlumno = idAlumno;
        this.carrera = carrera;
    }

    public int getIdAlumno() {
        return idAlumno;
    }

    public void setIdAlumno(int idAlumno) {
        this.idAlumno = idAlumno;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public void MostrarNomAlumno()
    {
        super.MostrarNombre();
    }
}


public class Curso {
    private int idCurso;
    private String nombre;

    public Curso(int idCurso, String nombre) {
        this.idCurso = idCurso;
        this.nombre = nombre;
    }

    public int getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}

public interface iMateriales {

    public void RegistrarMaterial(int id, String nombre);

    public String MostrarMaterial();
}

public class Materiales implements iMateriales {

    @Override
    public void RegistrarMaterial(int id, String nombre) {

    }

    @Override
    public String MostrarMaterial() {
        return null;
    }
}


public class Persona {
    private int idPersona;
    private String nombre;
    private String apPaterno;
    private String apMaterno;

    public Persona(int idPersona, String nombre, String apPaterno, String apMaterno) {
        this.idPersona = idPersona;
        this.nombre = nombre;
        this.apPaterno = apPaterno;
        this.apMaterno = apMaterno;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApPaterno() {
        return apPaterno;
    }

    public void setApPaterno(String apPaterno) {
        this.apPaterno = apPaterno;
    }

    public String getApMaterno() {
        return apMaterno;
    }

    public void setApMaterno(String apMaterno) {
        this.apMaterno = apMaterno;
    }

    public void MostrarNombre()
    {
        System.out.println("El nombre es: "+ getNombre()+" "+getApPaterno()+" "+getApMaterno());
    }
}



public class Profesores {

    private int idProfesor;
    private String nombre;
    private String apPaterno;
    private String apMaterno;

    public Profesores(int idProfesor, String nombre, String apPaterno, String apMaterno) {
        this.idProfesor = idProfesor;
        this.nombre = nombre;
        this.apPaterno = apPaterno;
        this.apMaterno = apMaterno;
    }

    public int getIdProfesor() {
        return idProfesor;
    }

    public void setIdProfesor(int idProfesor) {
        this.idProfesor = idProfesor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApPaterno() {
        return apPaterno;
    }

    public void setApPaterno(String apPaterno) {
        this.apPaterno = apPaterno;
    }

    public String getApMaterno() {
        return apMaterno;
    }

    public void setApMaterno(String apMaterno) {
        this.apMaterno = apMaterno;
    }
}




public class MyClass {
    public static void main(String[] args)
    {
        Alumno objAlumno= new Alumno (5689,"Juan","Perez","Mendez",8965,"Contaduria");

        objAlumno.MostrarNomAlumno();
    }
}
