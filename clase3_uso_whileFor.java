package usoWhile;

public class usowhile {
    public static void main(String[]args){
        int aux=150;
        while (aux<=100)
        {
            System.out.println("El numero es "+aux);
            aux++;
        }

        //do While
        int aux2=330;
        do{
            System.out.println("El numero es " + aux2);
            aux2++;
        }while (aux2<=100);

        //La tabla de multiplicar
        int tabla=9;
        int aux3=1;
        while (aux3<=10)
        {
            System.out.println(tabla + " x " + aux3 + " = " + (tabla*aux3));
            aux3++;
        }

        //Uso de For
        /*int m=11;
        for (int i=1;i<=10;i++)
        {
            System.out.println(m+" x "+ i + " = " + (i*m));
        }*/

        //100 primeros pares
        for (int i=1;i<=100;i++)
        {
            if (i % 2==0)
                System.out.println(i);
        }

        //numeros de 1 al 100 (ambos incluidos) divisibles entre 2 o 3. utilizar bucle que se desee.
        for (int i=1;i<=100;i++)
        {
            if(i%2==0 && i%3==0)
                System.out.println(i);
        }


    }
}
