public class Curso {
    private int idCurso;
    private String nombre;
    private String fecha_ini;
    private double precio;
    private String estado;
    private String categoria;

    public Curso(int idCurso, String nombre, String fecha_ini, double precio, String estado) {
        this.idCurso = idCurso;
        this.nombre = nombre;
        this.fecha_ini = fecha_ini;
        this.precio = precio;
        this.estado = estado;

        if (precio<1000)
            this.categoria="Sin categoria";
        else if (precio>=2000)
            this.categoria="Gold";
        else
            this.categoria="Silver";

    }

    public int getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha_ini() {
        return fecha_ini;
    }

    public void setFecha_ini(String fecha_ini) {
        this.fecha_ini = fecha_ini;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}





public class MyClass {
    public static void main(String[] args)
    {
        Curso cursoAND1=new Curso(1,"Android 1","21/03/2019",1500.00,"Vigente");
        System.out.println(cursoAND1.getCategoria());
    }

}