package com.developerti.escsrl.aplicacioninicialand1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.developerti.escsrl.aplicacioninicialand1.Negocio.Persona;

public class MainActivity extends AppCompatActivity {

    TextView texto;
    Button boton;
    EditText edTexto;
    EditText edTexto2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        texto=(TextView) findViewById(R.id.texto);
        boton=(Button) findViewById(R.id.boton);
        edTexto=(EditText) findViewById(R.id.editTexto);
        edTexto2=(EditText) findViewById(R.id.editTexto2);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean opcion=true;
                if (opcion)
                {
                    int resultado=suma(Integer.parseInt(edTexto.getText().toString()), Integer.parseInt(edTexto2.getText().toString()));
                    texto.setText("El resultado de la suma es: "+ String.valueOf(resultado));
                }
                else
                {
                    Persona cliente=new Persona("Marco Antonio","75583225");
                    texto.setText("Ud es el cliente: "+cliente.getNombre()+ " "+cliente.getNroTelf());
                }
            }
        });
    }

    public int suma(int num1, int num2)
    {
        return num1+num2;
    }

}
