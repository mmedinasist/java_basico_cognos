import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MyClass2 {

    public static void main(String[] args){
        /*Empleado emp1=new Empleado(1,"Juan",100);

        System.out.println(emp1.getSueldo());
        emp1.aumentarSueldo(20);
        System.out.println(emp1.getSueldo());*/
        Empleado[] empleados=new Empleado[3];
        empleados[0]=new Empleado(1,"Juan Garcia", 150);
        empleados[1]=new Empleado(2,"Pablo Juarez",200);
        empleados[2]=new Empleado(3,"Daniel Salamanca",300);

        try
        {
            ObjectOutputStream escribiendotxt=new ObjectOutputStream(new FileOutputStream("G:/pruebaText/archivo.txt"));
            escribiendotxt.writeObject(empleados);
            escribiendotxt.close();

            ObjectInputStream leer_fichero=new ObjectInputStream(new FileInputStream("G:/pruebaText/archivo.txt"));
            Empleado[] empleados2=(Empleado[]) leer_fichero.readObject();
            leer_fichero.close();

            for (Empleado e: empleados2)
            {
                System.out.println(e.getNombre());
            }
        }
        catch (Exception ex)
        {}
    }
}