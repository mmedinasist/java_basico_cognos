public class Computadora {
    private String procesador;
    private String memoria;
    private String sistOp;
    private String discoDuro;
    private String tarjVid;
    private double precio;

    public Computadora(String procesador, String memoria, String sistOp, String discoDuro, String tarjVid, double precio) {
        this.procesador = procesador;
        this.memoria = memoria;
        this.sistOp = sistOp;
        this.discoDuro = discoDuro;
        this.tarjVid = tarjVid;
        this.precio = precio;
    }

    public String getProcesador() {
        return procesador;
    }

    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    public String getMemoria() {
        return memoria;
    }

    public void setMemoria(String memoria) {
        this.memoria = memoria;
    }

    public String getSistOp() {
        return sistOp;
    }

    public void setSistOp(String sistOp) {
        this.sistOp = sistOp;
    }

    public String getDiscoDuro() {
        return discoDuro;
    }

    public void setDiscoDuro(String discoDuro) {
        this.discoDuro = discoDuro;
    }

    public String getTarjVid() {
        return tarjVid;
    }

    public void setTarjVid(String tarjVid) {
        this.tarjVid = tarjVid;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
