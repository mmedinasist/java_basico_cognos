import java.util.HashMap;
import java.util.Map;

public class MyClass {
    public static void main(String [] args)
    {
        Map<Integer,String> lstMap=new HashMap<Integer, String>();
        lstMap.put(5,"Jose Carlos");
        lstMap.put(6,"Marco Antonio");
        lstMap.put(7,"Juan Perez");

        System.out.println("Alumno: "+lstMap.get(7));
        //System.out.println(lstMap.size());
        /*lstMap.size();
        lstMap.clear();
        lstMap.remove(7);*/
        //System.out.println(lstMap.isEmpty());

        Map<Integer,Persona> lstPersona=new HashMap<Integer, Persona>();
        lstPersona.put(1,(new Persona(1,"Donald Trump")));
        lstPersona.put(2,(new Persona(2,"Pablo Suarez")));

        Persona objNPersona=lstPersona.get(2);
        System.out.println(objNPersona.getIdPersona()+ " "+ objNPersona.getNombre());
    }


    public static class Persona{
        private int idPersona;
        private String nombre;

        public Persona(int idPersona, String nombre) {
            this.idPersona = idPersona;
            this.nombre = nombre;
        }

        public int getIdPersona() {
            return idPersona;
        }

        public void setIdPersona(int idPersona) {
            this.idPersona = idPersona;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }
    }
}