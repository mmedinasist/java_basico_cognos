//Sist ERP

public class Almacen {
    private int idAlmacen;
    private String ubicacion;

    public Almacen(int idAlmacen, String ubicacion) {
        this.idAlmacen = idAlmacen;
        this.ubicacion = ubicacion;
    }

    public int getIdAlmacen() {
        return idAlmacen;
    }

    public void setIdAlmacen(int idAlmacen) {
        this.idAlmacen = idAlmacen;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}


public class Categoria {
    private int idCategoria;
    private String nombreCateg;

    public Categoria(int idCategoria, String nombreCateg) {
        this.idCategoria = idCategoria;
        this.nombreCateg = nombreCateg;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombreCateg() {
        return nombreCateg;
    }

    public void setNombreCateg(String nombreCateg) {
        this.nombreCateg = nombreCateg;
    }
}


public class Insumos extends Producto {
    private int idInsumo;
    private String uso;

    public Insumos(int idproducto, String nombre, String descripcion, int stock, int idInsumo, String uso) {
        super(idproducto, nombre, descripcion, stock);
        this.idInsumo = idInsumo;
        this.uso = uso;
    }

    public int getIdInsumo() {
        return idInsumo;
    }

    public void setIdInsumo(int idInsumo) {
        this.idInsumo = idInsumo;
    }

    public String getUso() {
        return uso;
    }

    public void setUso(String uso) {
        this.uso = uso;
    }

    public void MostrarInsumo()
    {
        super.MostrarProducto();
    }
}



public class Producto {
    private int idproducto;
    private String nombre;
    private String descripcion;
    private int stock;

    public Producto(int idproducto, String nombre, String descripcion, int stock) {
        this.idproducto = idproducto;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.stock = stock;
    }

    public int getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(int idproducto) {
        this.idproducto = idproducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void MostrarProducto()
    {
        System.out.println("El producto es: "+getNombre()+" "+getDescripcion()+" "+getStock());
    }
}




public class ProdVenta extends Producto{
    private int idProVenta;
    private Categoria objCategoria;
    private Almacen objAlmacen;

    public ProdVenta(int idproducto, String nombre, String descripcion, int stock, int idProVenta, Categoria objCategoria, Almacen objAlmacen) {
        super(idproducto, nombre, descripcion, stock);
        this.idProVenta = idProVenta;
        this.objCategoria = objCategoria;
        this.objAlmacen = objAlmacen;
    }

    public int getIdProVenta() {
        return idProVenta;
    }

    public void setIdProVenta(int idProVenta) {
        this.idProVenta = idProVenta;
    }

    public Categoria getObjCategoria() {
        return objCategoria;
    }

    public void setObjCategoria(Categoria objCategoria) {
        this.objCategoria = objCategoria;
    }

    public Almacen getObjAlmacen() {
        return objAlmacen;
    }

    public void setObjAlmacen(Almacen objAlmacen) {
        this.objAlmacen = objAlmacen;
    }

    public void MostrarProdVenta()
    {
        super.MostrarProducto();
    }
}



public class MyClass {
    public static void main(String[]args)
    {
        Categoria objCategor=new Categoria(1,"Impresoras");
        Almacen objAlmacen=new Almacen(1,"Av. Viedma");
        ProdVenta prodVenta=new ProdVenta(1,"HP 02","Laser",5,1,objCategor,objAlmacen);

        prodVenta.MostrarProdVenta();

    }
}

