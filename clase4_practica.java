package com.escsrl.aplicacion.pooclase4;

public class Automovil {

    private int chasis;
    private String placa;
    private String marca;
    private String modelo;
    private double precio;
    private boolean vendido;


    //CONSTRUCTOR

    public Automovil(int chasis, String placa, String marca, String modelo, double precio, boolean vendido) {
        this.chasis = chasis;
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.precio = precio;
        this.vendido = vendido;
    }

    //METODO
    public void Oferta()
    {
        setPrecio(getPrecio()*0.8);

    }

    public void vendido()
    {
        setVendido(true);
    }

    //METODOS GET AND SET
    public int getChasis() {
        return chasis;
    }

    public void setChasis(int chasis) {
        this.chasis = chasis;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public boolean isVendido() {
        return vendido;
    }

    public void setVendido(boolean vendido) {
        this.vendido = vendido;
    }
}
