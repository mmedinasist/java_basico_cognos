public class MyClass {
    public static void main(String [] args)
    {
        int aux=3;
        if (aux==1)
            System.out.println("Estamos en el mes: Enero");
        else if (aux==2)
            System.out.println("Estamos en el mes: Febrero");
        else if (aux==3)
            System.out.println("Estamos en Marzo");
        else if (aux==4)
            System.out.println("Estamos e Abril");
        else if (aux==5)
            System.out.println("Estamos en Mayo");
        else if (aux==6)
            System.out.println("Estamos en Junio");
        else
            System.out.println("Estamos en Julio u otro mes");


        //Uso de Switch
        switch (aux)
        {
            case 1:
                System.out.println("Estamos en enero");
                break;
            case 2:
                System.out.println("Estamos en el mes: Febrero");
                break;
            case 3:
                System.out.println("Estamos en Marzo");
                break;
            case 4:
                System.out.println("Estamos e Abril");
                break;
            case 5:
                System.out.println("Estamos en Mayo");
                break;
            case 6:
                System.out.println("Estamos en Junio");
                break;
            case 7:
                System.out.println("Estamos en Julio");
                break;
            case 8:
                System.out.println("Estamos en Agosto");
                break;
            case 9:
                System.out.println("Estamos en Septiembre");
                break;
            case 10:
                System.out.println("Estamos en Octubre");
                break;
            case 11:
                System.out.println("Estamos en noviembre");
                break;
            case 12:
                System.out.println("Estamos en diciembre");
                break;
            default:
                 System.out.println("No corresponde a ningun  mes");
                    break;
        }
    }
}